import React, { Component } from 'react';
import $ from 'jquery'
import './App.css';

class CreatePost extends Component {
  constructor(props) {
    super(props)
    this.state = {newposttitle: ""}
    this.handleAddPost = this.handleAddPost.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleAddPost(e){
    console.log(this.state.newposttitle)
  }
  handleChange(event) {
    this.setState({newposttitle: event.target.value});
  }
  render() {
    return (
            <div>
            <input type="text" value={this.state.newposttitle} onChange={this.handleChange}/>
            <input type="button" value="add" onClick={this.handleAddPost} />
            </div>
            )
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    }
  }
  componentDidMount() {
    $.ajax({
      url: "http://127.0.0.1:8000/posts/",
      dataType: 'json',
      success: function(posts) {
        this.setState({posts: posts});
      }.bind(this)
    });
  }

  renderPost({id, title}) {
    return <li key={id}>{id}—{title}</li>;
  }

  render() {
    return (
            <div>
            <ul> {this.state.posts.map(this.renderPost)} </ul>
            <CreatePost />
            </div>

            );
  }
}

export default App;
